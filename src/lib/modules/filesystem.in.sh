#!/bin/bash
[ -n "$DEVICE_INSTALL_FILESYSTEM" ] && return
DEVICE_INSTALL_FILESYSTEM=1

erase_dev()
{
    dd if=/dev/zero of="$1"
}


apply_fslayout()
{
    parted < "$1"
}


setup_lvm()
{
    (
        load "$1"
    )
}

mount_filesystems()
{
    (
        load "$1"
    )
}
