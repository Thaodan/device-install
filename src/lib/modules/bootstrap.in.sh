#!/bin/bash

bootstrap()
{
    root=$1
    shift
    pacstrap -C $pacman_conf "$root" "$@"
}


repo_add()
{
    repo_name=$1
    repo_url=$2
    shift 2

    cat <<EOF
[$repo_name]
Server=$repo_url
EOF
}
