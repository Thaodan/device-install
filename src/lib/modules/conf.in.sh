#!/bin/bash
[ -n "$DEVICE_INSTALL_CONF" ] && return
DEVICE_INSTALL_CONF=1
# vars:
# TEMPLATE_PATH

source @libdir@/save_ext
source @libdir@/message
source @libdir@/modules/template_spec

load()
{
    # sources file from template path
    # $1 file to load
    local file=$1
    shift
    if [ ! -e $file ] ; then
        # only search for $file if not found in the current dir tree
        case $file in
            /*) error "cant load $file: file not found" ; exit 1;;
            *)
                local dir
                for dir in ${TEMPLATE_PATH[*]} ; do
                    if [ -e "$dir/$file" ] ; then
                        file="$dir/$file"
                    fi
                done
                if [ ! -e $file ] ; then
                    error "cant load $file: file not found"
                    exit 1
                fi
                ;;
        esac
    fi
    if [ -d $file ] ; then
        if [ -e $file/DEVBUILD ] ; then
            file=$file/DEVBUILD
        else
            error "$file is a dir but contains no dev build"
            exit 1
        fi
    fi
    if ! sane_check $file ; then
        error "syntax error"
    fi
    
    srcdir=$(dirname $file)
    case $file in
        /*)  : ;;
        *) srcdir=$(realpath "$srcdir") ;;
    esac
    TEMPLATE_PATH+=("$srcdir")
    source_safe $file "$@"
    env_sav
}

# check if current template tree is valid
env_valid_check()
{
    local nwords
    for word in $(template_req_keywords) ; do
        if eval "[ ! \$$word ]" ; then
            nwords+=($word)
        fi
    done
    
    if [  ${#nwords} -gt 0 ] ; then
        error "${nwords[*]} not defined in current template tree"
        exit 1
    fi
}

env_sav()
{
    case "$part_run" in
        /*) : ;;
        *)  part_run="$srcdir/$part_run" ;;
    esac

    case "$sparse" in
        /*) : ;;
        *) sparse="$srcdir/$sparse" ;;
    esac

    case "$past_bootstrap_run" in
        /*) : ;;
        *) past_bootstrap_run="$srcdir/$past_bootstrap_run" ;;
    esac

    case "$extra_repos" in
        /*) : ;;
        *) extra_repos="$srcdir/$extra_repos" ;;
    esac
}
