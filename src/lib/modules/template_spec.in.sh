#!/bin/bash
[ -n "$DEVICE_INSTALL_TEMPLATESPEC" ] && return
DEVICE_INSTALL_TEMPLATESPEC=1

template_req_keywords()
# keyword       description
# machinename - name of the specific device
# packages    - (meta) packages to install
# part_run    - script or scripts to run to setup partions
{
    cat <<EOF
machinename
packages
part_run
EOF
}

template_opt_keywords()
# keyword            - description
# post_bootstrap_run - script to run
#                      after bootstrap in chroot
# ersase_devices     - devices to erase
# extra_repo         - and an extra repo beside the offical
#                    - arch repos
{
    cat <<EOF
sparse
post_bootstrap_run
erase_devices
extra_repos
EOF
}
