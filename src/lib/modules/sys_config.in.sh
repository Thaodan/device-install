#!/bin/bash


chroot_run_script()
{
    # run script in chroot
    cp "$1" $CHROOTDIR/$(basename "$1")
    arch-chroot $CHROOTDIR  $CHROOTDIR/$(basename "$1")
}
