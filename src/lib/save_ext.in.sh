#!/bin/bash
[ -n "$DEVICE_INSTALL_SAVE_EXT" ] && return
DEVICE_INSTALL_SAVE_EXT=1

source_safe() {
    shopt -u extglob
    set -o errexit
    if ! source "$@"; then
	error "$(gettext "Failed to source %s")" "$1"
	exit 1
    fi
    shopt -s extglob
    set +o errexit
}


sane_check()
{
    bash -n "$@"
}
