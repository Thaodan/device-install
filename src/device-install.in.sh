#!/bin/bash
VER=0.1

CHROOTDIR=/mnt
pac_template=@libdir@/pacman.template

source @libdir@/message
source @libdir@/parseopts
source @libdir@/modules/conf
source @libdir@/modules/filesystem
source @libdir@/modules/bootstrap
source @libdir@/modules/sys_config

usage()
{
    cat <<EOF
Usage: $appname --chroot-dir <dir> device-template

Options: 
--chroot-dir set CHROOTDIR for install
--pac-template use another pacman template
--skip         skip a certain task like:
               part - dont run partion scripts 
               past_bootstrap_run - dont run past bootstrap scripts
               sparse - dont copy sparse files to install root
               bootstrap - dont bootstrap with pacman
               this option can be given multiple times

--help -h  show this help messsage
--version -v show version 
EOF
}
OPT_SHORT='hv'
OPT_LONG=('help' 'version' 'dry-run' 'chroot-dir:' 'pac-template:' 'skip:')

if ! parseopts "$OPT_SHORT" "${OPT_LONG[@]}" -- "$@"; then
    exit 1
fi
set -- "${OPTRET[@]}"
unset OPT_LONG OPT_SHORT
while true ; do
    case "$1" in
        -h|--help) usage ; exit 0;;
        -v|--version) echo $VER;;
        --chroot-dir ) shift; CHROOTDIR="$1" ;;
        --pac-template) shift; pac_template="$1" ;;
        --skip)
            shift
            case $1 in
                part) part_run_s=skip;;
                past_bootstrap_run)  past_bootstrap_run_s=skip ;;
                sparse) sparse_s=skip;;
                bootstrap) bootstrap_s=skip ;;
                *) error "Unkown option for skip valid words are part,bootstrap_run,sparse,bootstrap"
                   exit 1
                   ;;
            esac
            ;;
        --|-) shift ;break;;
    esac
    shift
done

(( EUID == 0 )) || {
    error 'This script must be run with root privileges'
    exit 1
} 
TEMPLATE_PATH+=("$PWD")

root_template="$1"
shift

if [ ! -r "$root_template" ] ; then
    error "${root_template:-root-template} not found"
    exit 1
fi

load "$root_template"
env_valid_check

if [ -n "$erase_devices" ] ; then
    for dev in ${erase_devices[*]} ; do
        if ! erase_dev $dev; then
            error "error while erasing $dev"
            exit 1
        fi
    done
    unset dev
fi

if [ ! "$part_run_s" = skip ] ; then
    if [ ! -e "$part_run" ] ; then
        error "not part_run found or set"
        exit 1
    elif [ -f "$part_run" ] ; then
        load "$part_run"
    elif [ -d "$part_run" ] ; then
        if [ -z "$(ls $part_run/*)" ] ; then
            error "no part scripts found"
            exit
    fi
        for script in "$part_run"/* ; do
            if [ -x "$script" ] ; then
                load "$script"
            fi
        done
    fi
fi


pacman_conf=$(mktemp)
cat "$pac_template" > $pacman_conf
if [ -e "$extra_repos" ] ; then
    if [ -d "$extra_repos" ] ; then
        for repo in ${extra_repos}/*; do
            (
                load "$repo"
                repo_add "$repo_name" "$repo_url" >> $pacman_conf
            )
        done
    else
        error "extra_repos must be dir"
        exit 1
    fi
fi
# bootstrap
if [ ! "$bootstrap_s" = skip ] ; then
    bootstrap $CHROOTDIR  $packages
fi
# gen fstab
genfstab -L $CHROOTDIR > $CHROOTDIR/etc/fstab

# set hostname 
printf %s $machinename > $CHROOTDIR/etc/hostname

if [ "$sparse" ] && [ ! "$sparse_s" = skip ] ; then
    if [ ! -d $sparse ] ; then
        error "sparse must be a directory"
        exit 1
    fi
fi


if [ "$past_bootstrap_run" ] && [ ! "$past_bootstrap_run_s" = skip  ]; then
    if [ -d "$past_bootstrap_run" ] ; then
        if [ -z "$(ls $past_bootstrap_run/*)" ] ; then
           error "no past_bootstrap_run scripts found but variable set"
           exit 1
        fi
        for script in "$past_bootstrap_run"/* ; do
            if [ -x "$script" ] ; then
                load "$script"
            fi
        done
    elif [ -f "$past_bootstrap_run" ] ; then
        chroot_run_script  "$past_bootstrap_run"
    fi
fi

cat $pacman_conf > $CHROOTDIR/etc/pacman.conf
if [ "$sparse" ] && [ ! "$sparse_s" = skip ] ; then
    cp --no-dereference \
       --preserve=all \
       --recursive \
       "$sparse"/* $CHROOTDIR
fi
