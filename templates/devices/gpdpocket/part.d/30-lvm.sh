#!/bin/sh

pvcreate -ff /dev/mapper/LVM


vgcreate VolGroup00 /dev/mapper/LVM

# create system partition
lvcreate -L 20G VolGroup00 -n System
# create home partition
lvcreate -L 80G VolGroup00 -n Home
lvcreate -L 8G VolGroup00 -n Swap

mkfs.f2fs /dev/mapper/VolGroup00-System 
mkfs.f2fs /dev/mapper/VolGroup00-Home
mkswap /dev/mapper/VolGroup00-Swap
