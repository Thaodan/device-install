#!/bin/sh
mount /dev/mapper/VolGroup00-System "$CHROOTDIR"
mkdir -p "$CHROOTDIR/boot"

mount /dev/mmcblk0p1 "$CHROOTDIR"/boot
mkdir "$CHROOTDIR"/home
mount /dev/mapper/VolGroup00-Home "$CHROOTDIR"/home
