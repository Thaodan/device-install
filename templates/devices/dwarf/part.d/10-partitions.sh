#!/bin/sh

parted --script /dev/sdb  \
       mklabel gpt \
       mkpart primary fat32 '0%' 200MB\
       name 1 EFI-System \
       set 1 esp on \
       set 1 boot on

parted --script /dev/sdb \
       mkpart primary ext4 201MB 100%\
       name 2 Lvm-Home \
       set 2 lvm on


parted --script /dev/sda \
       mkpart primary ext4 '0%' '100'% \
       name 1 Lvm-System \
       set  1 lvm on

mkfs.vfat -F32 /dev/sdb1

