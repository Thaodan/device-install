#!/bin/sh

pvcreate -ff /dev/sdb2
pvcreate -ff /dev/sda1

vgcreate VolGroup00 /dev/sdb2 /dev/sda1

# create system partition
lvcreate -L 50G VolGroup00 -n System /dev/sda1
# create home partition
lvcreate -L 931G VolGroup00 -n Home /dev/sdb2
lvcreate -L 50G VolGroup00 -n Cache_Home /dev/sda1
lvcreate -L 50M VolGroup00 -n Cache_Home_meta /dev/sda1

lvconvert --type cache-pool --poolmetadata VolGroup00/Cache_Home_meta \
          VolGroup00/Cache_Home \
          --chunksize 512k


lvconvert --type cache --cachepool VolGroup00/Cache_Home \
          --cachemode writethrough VolGroup00/Home 

mkfs.f2fs /dev/mapper/VolGroup00-System 
mkfs.ext4 /dev/mapper/VolGroup00-Home
