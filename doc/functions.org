#+TITLE: Functions to use in template

* Intro
  These functions can be used to achiev certain tasks in a template.
  Unlike keywords they're never required and are executed and not just
  set.


* load
** Usage 
#+BEGIN_SRC sh
load <file>
#+END_SRC
** Description
   Load another template

